using System.Collections;
using NUnit.Framework;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.TestTools;

public class UnitTests
{
    private GameObject _gameObject;
    private World _world;
    private EntityManager _entityManager;

    [SetUp]
    public void SetUp()
    {
        _gameObject = new GameObject("TestObject");
        _world = World.DefaultGameObjectInjectionWorld;
        _entityManager = _world.EntityManager;
    }

    [UnityTest]
    public IEnumerator CharacterLevelUpTest()
    {
        _gameObject.AddComponent<CharacterData>();

        var oldLevel = _gameObject.GetComponent<CharacterData>().CurrentLevel;

        _gameObject.GetComponent<CharacterData>().AddScore(40);

        var newLevel = _gameObject.GetComponent<CharacterData>().CurrentLevel;

        UnityEngine.Assertions.Assert.AreNotEqual(oldLevel, newLevel);
        yield return null;
    }

    [UnityTest]
    public IEnumerator CharacterPickUpScoreTest()
    {
        _gameObject.AddComponent<CharacterData>();

        var go = new GameObject();

        go.AddComponent<PickUpScoreAbility>();
        go.GetComponent<PickUpScoreAbility>().Targets.Add(_gameObject);

        var oldScore = _gameObject.GetComponent<CharacterData>().Score;

        go.GetComponent<PickUpScoreAbility>().Execute(Entity.Null);

        var newScore = _gameObject.GetComponent<CharacterData>().Score;

        UnityEngine.Assertions.Assert.AreNotEqual(oldScore, newScore);
        yield return null;
    }

    [UnityTest]
    public IEnumerator CharacterEntityMoveTest()
    {
        var ent = _entityManager.CreateEntity();
        _entityManager.AddComponentData(ent, new InputData()
        {
            move = new float2(1.0f, 1.0f)
        });
        _entityManager.AddComponentData(ent, new MoveData()
        {
            speed = new float2(0.2f, 0.2f)
        });

        GameObject go = new GameObject();
        Transform transform = go.transform;

        Vector3 oldPosition = transform.position;

        _entityManager.AddComponentObject(ent, transform);
        _world.Update();
        int i = 0;
        while (i < 1000)
        {
            _world.Update();
            i++;
            yield return null;
        }
        Transform newTransform = _entityManager.GetComponentObject<Transform>(ent);

        UnityEngine.Assertions.Assert.AreNotEqual(oldPosition, newTransform.position);
    }

    [UnityTest]
    public IEnumerator BulletEntityMoveTest()
    {

        var ent = _entityManager.CreateEntity();
        _entityManager.AddComponentData(ent, new MoveData()
        {
            speed = new float2(0.2f, 0.2f)
        });


        _gameObject.AddComponent<BulletConvertToEntity>();

        _entityManager.AddComponentObject(ent, _gameObject.transform);
        _entityManager.AddComponentData(ent, new Translation()
        {
            Value = new float3(0, 0, 0)
        });
        _entityManager.AddComponentData(ent, new Rotation()
        {
            Value = new Quaternion(0, 0, 0, 0)
        });
        _entityManager.AddComponentObject(ent, _gameObject.GetComponent<BulletConvertToEntity>());


        float3 oldPosition = _entityManager.GetComponentData<Translation>(ent).Value;

        _world.Update();
        int i = 0;
        while (i < 1000)
        {
            _world.Update();
            i++;
            yield return null;
        }
        float3 newPosition = _entityManager.GetComponentData<Translation>(ent).Value;

        UnityEngine.Assertions.Assert.AreNotEqual(oldPosition, newPosition);
    }

    [UnityTest]
    public IEnumerator AnimationTest()
    {
        var ent = _entityManager.CreateEntity();
        _entityManager.AddComponentData(ent, new HealthRecoverAnimData());
        _entityManager.AddComponentData(ent, new Translation());
        _entityManager.AddComponentObject(ent, _gameObject.transform);

        float3 oldPosition = _entityManager.GetComponentData<Translation>(ent).Value;

        _world.Update();

        yield return new WaitForSeconds(0.5f);

        float3 newPosition = _entityManager.GetComponentData<Translation>(ent).Value;
        UnityEngine.Assertions.Assert.AreNotEqual(oldPosition, newPosition);

    }
}