﻿using DG.Tweening;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class HealthRecoverAnimSystem : ComponentSystem
{
    private EntityQuery _animQuery;

    protected override void OnCreate()
    {
        _animQuery = GetEntityQuery(ComponentType.ReadOnly<HealthRecoverAnimData>(),
            ComponentType.ReadOnly<Transform>());
    }

    protected override void OnStartRunning()
    {
        Entities.With(_animQuery).ForEach(
            (Entity entity, Transform transform, ref Translation translation) =>
            {
                Sequence sequence = DOTween.Sequence();
                sequence.Append(transform.DOMoveY(transform.position.y + 0.5f, 1.0f).OnUpdate(() =>
                {
                    if (transform == null)
                    {
                        sequence.Kill();
                        return;
                    }
                    World.DefaultGameObjectInjectionWorld.EntityManager.SetComponentData(entity, new Translation()
                    {
                        Value = transform.position
                    });
                }));
                sequence.AppendInterval(0.5f);
                sequence.SetLoops(-1, LoopType.Yoyo);
            });
    }

    protected override void OnUpdate()
    {
        Entities.With(_animQuery).ForEach(
            (Entity entity, Transform transform) =>
            {
                
            });
    }
}
