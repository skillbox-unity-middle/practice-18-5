﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class CollisionSystem : ComponentSystem
{
    private EntityQuery _collisionQuery;

    private Collider[] _results = new Collider[50];
    protected override void OnCreate()
    {
        _collisionQuery = GetEntityQuery(ComponentType.ReadOnly<ActorColliderData>(),
            ComponentType.ReadOnly<Transform>());
    }

    protected override void OnUpdate()
    {
        Entities.With(_collisionQuery).ForEach(
            (Entity entity, CollisionAbility ability, ref Translation translation, ref Rotation rotationEntity, ref ActorColliderData colliderData) =>
            {
                float3 position = translation.Value;
                Quaternion rotation = rotationEntity.Value;
               
                ability.collisions.Clear();
                    
                int size = 0;

                switch (colliderData.colliderType)
                {
                    case ColliderType.Sphere:
                        size = Physics.OverlapSphereNonAlloc(colliderData.sphereCenter + position, 
                            colliderData.sphereRadius, _results);
                        break;
                    case ColliderType.Capsule:
                        var point1 = colliderData.capsuleStart + position;
                        var point2 = colliderData.capsuleEnd + position;
                        var center = (point1 + point2) / 2.0f;
                        point1 = (float3) (rotation * (point1 - center)) + center;
                        point2 = (float3) (rotation * (point2 - center)) + center;
                        size = Physics.OverlapCapsuleNonAlloc(point1, point2, colliderData.capsuleRadius, _results);
                        break;
                    case ColliderType.Box:
                        size = Physics.OverlapBoxNonAlloc(colliderData.boxCenter + position,
                            colliderData.boxHalfExtents, _results, colliderData.boxOrientation * rotation);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                if (size > 0)
                {
                    foreach (Collider col in _results)
                    {
                        foreach (var result in _results)
                        {
                            ability.collisions.Add(result);
                        }
                        ability.Execute(entity);
                    } 
                }
            });
    }
}