﻿using Unity.Entities;

public class CharacterShootSystem : ComponentSystem
{
    private EntityQuery _shootQuery;

    protected override void OnCreate()
    {
        _shootQuery = GetEntityQuery(ComponentType.ReadOnly<InputData>(),
            ComponentType.ReadOnly<ShootData>(),
            ComponentType.ReadOnly<CharacterConvertToEntity>());
    }

    protected override void OnUpdate()
    {
        Entities.With(_shootQuery).ForEach(
            (Entity entity, CharacterConvertToEntity character, ref InputData inputData) =>
            {

                if (character.shootAction != null && character.shootAction is IAbility ability)
                {
                    if (inputData.shoot > 0)
                    {
                        ability.Execute(entity);
                    }
                }
            });
    }
}
