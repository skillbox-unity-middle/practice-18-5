﻿using Unity.Entities;
using UnityEngine;

public class CharacterDashSystem : ComponentSystem
{
    private EntityQuery _dashQuery;

    protected override void OnCreate()
    {
        _dashQuery = GetEntityQuery(ComponentType.ReadOnly<InputData>(),
            ComponentType.ReadOnly<DashData>(),
            ComponentType.ReadOnly<CharacterConvertToEntity>(),
            ComponentType.ReadOnly<Transform>());
    }

    protected override void OnUpdate()
    {
        Entities.With(_dashQuery).ForEach(
            (Entity entity, CharacterConvertToEntity character, ref InputData inputData) =>
            {
                if (character.dashAction != null && character.dashAction is IAbility ability)
                {
                    if (inputData.dash > 0)
                    {
                        ability.Execute(entity);
                    }

                }
            });
    }
}
