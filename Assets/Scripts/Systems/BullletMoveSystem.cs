﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class BullletMoveSystem : ComponentSystem
{
    private EntityQuery _moveQuery;

    protected override void OnCreate()
    {
        _moveQuery = GetEntityQuery(ComponentType.ReadOnly<MoveData>(),
            ComponentType.ReadOnly<BulletConvertToEntity>(),
            ComponentType.ReadOnly<Transform>());
    }

    protected override void OnUpdate()
    {
        Entities.With(_moveQuery).ForEach(
            (Entity entity, ref Translation translation, ref Rotation rotation, ref MoveData moveData) =>
            {
                var pos = translation.Value;
                var rot = rotation.Value;

                float3 q = GetQuaternionEulerAngles(rot);

                pos += new float3( math.sin(q.y) * moveData.speed.x, 0, 
                    math.cos(q.y) * moveData.speed.y);
                translation.Value = pos;
            });
    }

    public float3 GetQuaternionEulerAngles(quaternion rot)
    {
        float4 q1 = rot.value;
        float sqw = q1.w * q1.w;
        float sqx = q1.x * q1.x;
        float sqy = q1.y * q1.y;
        float sqz = q1.z * q1.z;
        float unit = sqx + sqy + sqz + sqw;
        float test = q1.x * q1.w - q1.y * q1.z;
        float3 v;

        if (test > 0.4995f * unit)
        {
            v.y = 2f * math.atan2(q1.y, q1.x);
            v.x = math.PI / 2f;
            v.z = 0;
            return NormalizeAngles(v);
        }
        if (test < -0.4995f * unit)
        {
            v.y = -2f * math.atan2(q1.y, q1.x);
            v.x = -math.PI / 2;
            v.z = 0;
            return NormalizeAngles(v);
        }

        rot = new quaternion(q1.w, q1.z, q1.x, q1.y);
        v.y = math.atan2(2f * rot.value.x * rot.value.w + 2f * rot.value.y * rot.value.z, 1 - 2f * (rot.value.z * rot.value.z + rot.value.w * rot.value.w));
        v.x = math.asin(2f * (rot.value.x * rot.value.z - rot.value.w * rot.value.y));
        v.z = math.atan2(2f * rot.value.x * rot.value.y + 2f * rot.value.z * rot.value.w, 1 - 2f * (rot.value.y * rot.value.y + rot.value.z * rot.value.z));
        return NormalizeAngles(v);
    }

    float3 NormalizeAngles(float3 angles)
    {
        angles.x = NormalizeAngle(angles.x);
        angles.y = NormalizeAngle(angles.y);
        angles.z = NormalizeAngle(angles.z);
        return angles;
    }

    float NormalizeAngle(float angle)
    {
        while (angle > math.PI * 2f)
            angle -= math.PI * 2f;
        while (angle < 0)
            angle += math.PI * 2f;
        return angle;
    }
}

