﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class CharacterAnimSystem : ComponentSystem
{
    private EntityQuery _animQuery;

    protected override void OnCreate()
    {
        _animQuery = GetEntityQuery(ComponentType.ReadOnly<AnimDataConvertToEntity>(),
            ComponentType.ReadOnly<Animator>(),
            ComponentType.ReadOnly<InputData>());
    }

    protected override void OnUpdate()
    {
        Entities.With(_animQuery).ForEach(
            (Entity entity, ref InputData inputData, AnimDataConvertToEntity animData, Animator animator) =>
            {
                bool isDashAvalaible = World.DefaultGameObjectInjectionWorld.EntityManager.GetComponentData<DashData>(entity).isDashAvalaible;
                if(inputData.dash > 0 && isDashAvalaible)
                {
                    animator.SetBool(animData.dashAnimHash, true);
                    return;
                }
                else
                {
                    animator.SetBool(animData.dashAnimHash, false);
                    animator.SetFloat(animData.speedAnimHash, math.sqrt(math.pow(inputData.move.x, 2) + math.pow(inputData.move.y, 2)));
                }

            });
    }
}
