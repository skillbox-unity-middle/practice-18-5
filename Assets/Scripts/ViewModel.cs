using System.ComponentModel;
using UnityEngine;
using UnityWeld.Binding;

[Binding]
public class ViewModel : MonoBehaviour, INotifyPropertyChanged
{
    public event PropertyChangedEventHandler PropertyChanged;

    private string _health = "10";

    [Binding]
    public string Health
    {
        get => _health;
        set
        {
            if (_health.Equals(value)) return;
            _health = value;
            OnPropertyChanged(nameof(Health));
        }
    }

    private void OnPropertyChanged(string propertyName)
    {
        if(PropertyChanged != null)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
