using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Glow : MonoBehaviour
{
    [SerializeField, ColorUsage(true, true)] private Color _startColor;
    [SerializeField] private float _minPower;
    [SerializeField] private float _maxPower;
    [SerializeField] private float _glowSpeed = 1;
    private const string ColorKey = "_GlowColor";
    private const string PowerKey = "_GlowPower";
    private SkinnedMeshRenderer _renderer;
    private Coroutine _coroutine;

    void Start()
    {
        _renderer = GetComponent<SkinnedMeshRenderer>();
    }

    public void ShowGlow()
    {
        if (_coroutine != null)
        {
            StopCoroutine(_coroutine);
        }

        _coroutine = StartCoroutine(ShowGlowCoroutine());

    }

    private IEnumerator ShowGlowCoroutine()
    {
        float power = _minPower;

        _renderer.material.SetColor(ColorKey, _startColor);

        while (power < _maxPower)
        {
            power += Time.deltaTime * _glowSpeed;
            _renderer.material.SetFloat(PowerKey, power);
            yield return null;
        }

        _renderer.material.SetColor(ColorKey, Color.black);
        yield return null;
    }
}
