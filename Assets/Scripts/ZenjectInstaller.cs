using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

public class ZenjectInstaller : MonoInstaller
{
    [SerializeField] private GameBalance _balance;
    [SerializeField] private JsonGameBalance _jsonBalance;
    [SerializeField] private DummyGameBalance _dummyBalance;
    [SerializeField] SettingsFilesEnum DropDown = SettingsFilesEnum.Default;

    public override void InstallBindings()
    {
        switch (DropDown)
        {
            case SettingsFilesEnum.Json:
                Container.Bind<ISettings>().FromInstance(_jsonBalance).AsSingle().NonLazy();
                break;
            case SettingsFilesEnum.Dummy:
                Container.Bind<ISettings>().FromInstance(_dummyBalance).AsSingle().NonLazy();
                break;
            case SettingsFilesEnum.Scriptable:
                Container.Bind<ISettings>().FromInstance(_balance).AsSingle().NonLazy();
                break;
            default:
                Container.Bind<ISettings>().To<DummyGameBalance>().AsSingle().NonLazy();
                break;
        }
    }
}

public enum SettingsFilesEnum
{
    Json = 0,
    Dummy = 1,
    Scriptable = 2, 
    Default = 3
}