using UnityEngine;

public class DamageButton : MonoBehaviour
{
    [SerializeField] private Glow _glow;

    public void ClickGlowButton()
    {
        _glow.ShowGlow();
    }
}
