using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Settings : ScriptableObject
{
    public int heroHealth = 100;
    public int recoverHealth = 10;
}
