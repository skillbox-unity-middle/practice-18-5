using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CanEditMultipleObjects]
[CustomEditor(typeof(GameBalance))]
public class GamaBalanceEditorChanger : Editor
{
    private string[] _gameBalanceArray;
    private string[] _gameBalanceNameArray;
    private int _gameBalanceIndex;
    private SerializedProperty _heroHealth;
    private SerializedProperty _recoverHealth;

    private void OnEnable()
    {
        _heroHealth = serializedObject.FindProperty("_heroHealth");
        _recoverHealth = serializedObject.FindProperty("_recoverHealth");

        _gameBalanceArray = AssetDatabase.FindAssets("t:settings");
        _gameBalanceNameArray = new string[_gameBalanceArray.Length];
        for (int i = 0; i < _gameBalanceArray.Length; i++)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(_gameBalanceArray[i]);
            string assetName = assetPath.Split('/').Last();
            _gameBalanceNameArray[i] = assetName;
        }
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        Settings[] settings = new Settings[_gameBalanceArray.Length];

        for (int i = 0; i < settings.Length; i++)
        {
            settings[i] = AssetDatabase.LoadAssetAtPath<Settings>(AssetDatabase.GUIDToAssetPath(_gameBalanceArray[i]));
        }

        GUIContent arrayLabel = new GUIContent("Settings List");
        _gameBalanceIndex = EditorGUILayout.Popup(arrayLabel, _gameBalanceIndex, _gameBalanceNameArray);

        _heroHealth.intValue = settings[_gameBalanceIndex].heroHealth;
        _recoverHealth.intValue = settings[_gameBalanceIndex].recoverHealth;

        serializedObject.ApplyModifiedProperties();
    }
}
