﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

public class MoveBehaviour : MonoBehaviour, IBehaviour
{
    [SerializeField] private float _minDistance = 10;
    [SerializeField] private string _speedAnimHash;
    [SerializeField] private float _speedAnimMultiplier = 1.0f;
    private float _currentSpeedAnimMultiplier = 0.0f;
    private Vector3 _currentDestination;
    private NavMeshAgent _agent;
    private CharacterHealthConvertToEntity _character;
    private Animator _animtor;

    private void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _character = FindObjectOfType<CharacterHealthConvertToEntity>();
        _animtor = GetComponent<Animator>();
    }

    public void Behave()
    {
        gameObject.transform.LookAt(_character.transform);
    }

    public float Evaluate()
    {
        if (_character == null) return 0;
        float distance = (gameObject.transform.position - _character.transform.position).magnitude;
        if (distance < _minDistance)
        {
            if (distance > _agent.stoppingDistance)
            {
                return MoveOrStop(true);
            }
            else
            {
                return MoveOrStop(false);
            }
        }
        else
        {
            return MoveOrStop(false);
        }
    }

    private float MoveOrStop(bool isMoving)
    {
        if (isMoving)
        {
            _currentDestination = _character.transform.position;
            _agent.SetDestination(_currentDestination);
            SetAnimatorSpeed(_speedAnimMultiplier);
            return float.MaxValue;
        }
        else
        {
            _currentDestination = gameObject.transform.position;
            _agent.SetDestination(_currentDestination);
            SetAnimatorSpeed(0.0f);
            return float.MinValue;
        }
    }

    private void SetAnimatorSpeed(float currentSpeed)
    {
        _currentSpeedAnimMultiplier = currentSpeed;
        _animtor.SetFloat(_speedAnimHash, _currentSpeedAnimMultiplier);
    }
}

