﻿using Unity.Entities;
using UnityEngine;
using UnityEngine.AI;

public class AttackBehaviour : MonoBehaviour, IBehaviour, IConvertGameObjectToEntity
{
    [SerializeField] private ShootAbility _shootAbility;
    private NavMeshAgent _agent;
    private CharacterHealthConvertToEntity _character;
    private Entity _entity;

    private void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _character = FindObjectOfType<CharacterHealthConvertToEntity>();
    }

    public void Behave()
    {
        _shootAbility.Execute(_entity);
        gameObject.transform.LookAt(_character.transform);
    }

    public float Evaluate()
    {
        if (_character == null) return 0;
        float distance = (gameObject.transform.position - _character.transform.position).magnitude;
        if (distance <= _agent.stoppingDistance) return float.MaxValue;
        else return float.MinValue;
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        _entity = entity;
    }
}

