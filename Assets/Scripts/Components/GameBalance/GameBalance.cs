using System;
using UnityEngine;

public class GameBalance : MonoBehaviour, ISettings
{
    [SerializeField, ReadOnlyInInspector] private int _heroHealth;
    [SerializeField, ReadOnlyInInspector] private int _recoverHealth;
    private int _previousHeroHealth;
    private int _previousRecoverHealth;

    public int HeroHealth => _heroHealth;
    public int RecoverHealth => _recoverHealth;

    public event Action onLoadSettings;

    private void Start()
    {
        _previousHeroHealth = _heroHealth;
        _previousRecoverHealth = _recoverHealth;
        InvokeAction();
    }

    private void Update()
    {
        if (_heroHealth != _previousHeroHealth || _recoverHealth != _previousRecoverHealth)
        {
            _previousHeroHealth = _heroHealth;
            _previousRecoverHealth = _recoverHealth;
            InvokeAction();
        }
    }

    public void SettingsObjectChanged()
    {
        InvokeAction();
    }

    private void InvokeAction() 
    { 
        onLoadSettings?.Invoke(); 
    }
}
