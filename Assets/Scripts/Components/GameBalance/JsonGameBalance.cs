using System;
using UnityEngine;
using UniRx;
using System.IO;

public class JsonGameBalance : MonoBehaviour, ISettings
{
    private string _jsonSettingsFileName = "JsonSettings.json";
    private int _heroHealth = 300;
    private int _recoverHealth = 40;
    private string _dataFromJsonSettingFile;

    public int HeroHealth => _heroHealth;
    public int RecoverHealth => _recoverHealth;

    public event Action onLoadSettings;

    private void Start()
    {
        string dataPath = Application.dataPath;

        var loadSettingsFromJson = Observable.Start(() => {
            string path = dataPath + "/" + _jsonSettingsFileName;
            StreamReader reader = new StreamReader(path);
            _dataFromJsonSettingFile = reader.ReadToEnd();
            reader.Close();
        });

        Observable.SubscribeOnMainThread(loadSettingsFromJson).Subscribe(settings => {
            JsonSettigs settingsFromJson = JsonUtility.FromJson<JsonSettigs>(_dataFromJsonSettingFile);
            _heroHealth = settingsFromJson.heroHealth;
            _recoverHealth = settingsFromJson.recoverHealth;
            onLoadSettings?.Invoke();
        });
    }

}

[Serializable]
public class JsonSettigs
{
    public int heroHealth;
    public int recoverHealth;
}
