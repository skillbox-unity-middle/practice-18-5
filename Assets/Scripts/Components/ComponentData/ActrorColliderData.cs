﻿using Unity.Entities;
using Unity.Mathematics;

public struct ActorColliderData : IComponentData
{
    public ColliderType colliderType;
    public float3 sphereCenter;
    public float sphereRadius;
    public float3 capsuleStart;
    public float3 capsuleEnd;
    public float capsuleRadius;
    public float3 boxCenter;
    public float3 boxHalfExtents;
    public quaternion boxOrientation;
    public bool initialTakeOff;
}

public enum ColliderType
{
    Sphere = 0,
    Capsule = 1,
    Box = 2
}

