﻿using Unity.Entities;
using Unity.Mathematics;

public struct InputData : IComponentData
{
    public float2 move;
    public float dash;
    public float shoot;
}