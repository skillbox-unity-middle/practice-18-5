﻿using Unity.Entities;

public struct CharacterHealthData : IComponentData
{
    public int maxHealth;
    public int currentHealth;
}