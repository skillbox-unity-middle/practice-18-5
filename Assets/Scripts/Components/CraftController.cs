using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CraftController : MonoBehaviour
{
    private List<ICraftable> _items = new List<ICraftable>();
    private List<GameObject> _selectedList = new List<GameObject>();
    [SerializeField] private CraftSettings _craftSettings;
    [SerializeField] private Transform _inventoryUIRoot;

    public void EnterCraftMode()
    {
        _selectedList.Clear();
        _items = GetComponentsInChildren<ICraftable>().ToList();
        foreach (var item in _items)
        {
            if (!((MonoBehaviour)item).TryGetComponent(out Button button))
            {
                button = ((MonoBehaviour)item)?.gameObject.AddComponent<Button>();
                button.onClick.AddListener(() => { Select(button.gameObject); });
            }
        }
    }

    private void Select(GameObject obj)
    {
        if (_selectedList.Contains(obj))
        {
            _selectedList.Remove(obj);
            Color currentColor = obj.GetComponent<Image>().color;
            obj.GetComponent<Image>().color = new Color(currentColor.r, currentColor.b, currentColor.g, 1);
        }
        else
        {
            _selectedList.Add(obj);
            Color currentColor = obj.GetComponent<Image>().color;
            obj.GetComponent<Image>().color = new Color(currentColor.r, currentColor.b, currentColor.g, 0.5f);
        }

        CheckCombo();
    }

    public void CheckCombo()
    {
        List<string> selectedNames = new List<string>();
        foreach (var item in _selectedList)
        {
            var name = item.GetComponent<ICraftable>().Name;
            selectedNames.Add(name);
        }

        List<CraftCombination> combinations = _craftSettings.combinations;
        selectedNames.Sort();
        combinations.Sort();

        foreach (var combination in combinations)
        {
            if (combination.sources.SequenceEqual(selectedNames))
            {
                var targets = _selectedList[0].GetComponent<IAbilityTarget>().Targets;

                foreach (var victim in _selectedList)
                {
                    Destroy(victim);
                }

                var newItem = Instantiate(combination.result, _inventoryUIRoot);

                var ability = newItem.GetComponent<IAbilityTarget>();
                if (ability != null)
                {
                    foreach (var target in targets)
                    {
                        ability.Targets.Add(target);
                    }
                }
            }
        }
    }
}
