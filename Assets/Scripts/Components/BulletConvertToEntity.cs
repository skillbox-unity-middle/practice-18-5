﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class BulletConvertToEntity : MonoBehaviour, IConvertGameObjectToEntity
{
    [SerializeField] private float2 _speed = new float2(0.1f, 0.1f);

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponentData(entity, new MoveData()
        {
            speed = _speed
        });
        dstManager.AddComponentObject(entity, gameObject.transform);
        dstManager.AddComponentObject(entity, this);
        dstManager.AddComponentData(entity, new RotatedData()
        {
            isRotated = false   
        });
    }
}

