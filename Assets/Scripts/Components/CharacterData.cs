using System.Collections.Generic;
using UnityEngine;

public class CharacterData : MonoBehaviour
{
    public List<MonoBehaviour> levelUpActions = new List<MonoBehaviour>();
    public GameObject inventoryUIRoot;

    [SerializeField] private int _currentLevel = 1;
    [SerializeField] private int _score = 0;
    [SerializeField] private int _scoreToNextLevel = 20;
    private List<IItem> _items;

    public int CurrentLevel => _currentLevel;
    public int Score => _score;
    public int ScoreToNextLevel => _scoreToNextLevel;

    public void AddScore(int score)
    {
        _score += score;
        if (_score >= _scoreToNextLevel) LevelUp();
    }

    private void LevelUp()
    {
        _currentLevel++;
        _score = 0;
        _scoreToNextLevel *= 2;
        foreach (var action in levelUpActions)
        {
            if (action is not ILevelUp levelUp) return;
            levelUp.LevelUp();
        }
    }
}
