using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class PickUpItemToInventoryAbility : MonoBehaviour, IAbilityTarget, IConvertGameObjectToEntity, IItem
{
    [SerializeField] private GameObject _uIItem;

    private bool _isPickedUp = false;
    private EntityManager _dstManager;

    public List<GameObject> Targets { get; set; }
    public GameObject uIItem => _uIItem;

    public void Execute(Entity entity)
    {
        foreach (var target in Targets)
        {
            if (!_isPickedUp)
            {
                _isPickedUp = true;
                var character = target.GetComponent<CharacterData>();
                if (character == null) return;
                var item = Instantiate(uIItem, character.inventoryUIRoot.transform, false);

                var ability = item.GetComponent<IAbilityTarget>();
                ability?.Targets.Add(target);

                Destroy(gameObject);
                _dstManager.DestroyEntity(entity);
            }
        }
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        _dstManager = dstManager;
    }
}
