﻿using Unity.Entities;
using UnityEngine;

public class DashAbility : MonoBehaviour, IAbility
{
    [SerializeField] private float _range = 0.1f;
    [SerializeField] private float _dashDelay = 1.0f;
    [SerializeField] private AK.Wwise.Event _dashEvent;

    private float _dashTime = float.MinValue;

    public void Execute(Entity entity)
    {
        if (Time.time < _dashTime + _dashDelay)
        {
            World.DefaultGameObjectInjectionWorld.EntityManager.SetComponentData(entity, new DashData()
            {
                isDashAvalaible = false
            });
            return;
        }
        World.DefaultGameObjectInjectionWorld.EntityManager.SetComponentData(entity, new DashData()
        {
            isDashAvalaible = true
        });
        _dashTime = Time.time;

        _dashEvent.Post(gameObject);
        var pos = transform.position;
        pos += new Vector3(_range * Mathf.Sin(transform.eulerAngles.y * Mathf.Deg2Rad), 0, _range * Mathf.Cos(transform.eulerAngles.y * Mathf.Deg2Rad));
        transform.position = pos;
    }
}

