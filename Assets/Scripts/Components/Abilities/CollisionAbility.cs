using System;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class CollisionAbility : MonoBehaviour, IAbility, IConvertGameObjectToEntity
{
    [HideInInspector] public List<Collider> collisions;
    public List<MonoBehaviour> collisionActions = new List<MonoBehaviour>();

    [SerializeField] private Collider _collider;
    private List<IAbilityTarget> collisionActionsAbilities = new List<IAbilityTarget>();

    public void Execute(Entity entity)
    {
        foreach (var action in collisionActionsAbilities)
        {
            action.Targets = new List<GameObject>();
            collisions.ForEach(c =>
            {
                if (c != null) 
                {
                    action.Targets.Add(c.gameObject);
                }
            });
            action.Execute(entity); 
        }
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        foreach (var action in collisionActions)
        {
            if (action is IAbilityTarget ability)
            {
                collisionActionsAbilities.Add(ability);
            }
        }

        float3 position = gameObject.transform.position;
        switch (_collider)
        {
            case SphereCollider sphere:
                sphere.ToWorldSpaceSphere(out var sphereCenter, out var sphereRadius);
                dstManager.AddComponentData(entity, new ActorColliderData
                {
                    colliderType = ColliderType.Sphere,
                    sphereCenter = sphereCenter - position,
                    sphereRadius = sphereRadius,
                    initialTakeOff = true
                });
                break;
            case CapsuleCollider capsule:
                capsule.ToWorldSpaceCapsule(out var capsuleStart, out var capsuleEnd,
                    out var capsuleRadius);
                dstManager.AddComponentData(entity, new ActorColliderData
                {
                    colliderType = ColliderType.Capsule,
                    capsuleStart = capsuleStart - position,
                    capsuleEnd = capsuleEnd - position,
                    capsuleRadius = capsuleRadius,
                    initialTakeOff = true
                });
                break;
            case BoxCollider box:
                box.ToWorldSpaceBox(out var boxCenter, out var boxHalfExtents,
                    out var boxOrientation);
                dstManager.AddComponentData(entity, new ActorColliderData
                {
                    colliderType = ColliderType.Box,
                    boxCenter = boxCenter - position,
                    boxHalfExtents = boxHalfExtents,
                    boxOrientation = boxOrientation,
                    initialTakeOff = true
                });
                break;
        }

        dstManager.AddComponentObject(entity, this);

        _collider.enabled = false;
    }
}
