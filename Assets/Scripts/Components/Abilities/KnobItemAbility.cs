using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class KnobItemAbility : MonoBehaviour, IAbilityTarget, ICraftable
{
    [SerializeField] private int _hpPerUse = 10;
    [SerializeField] private string _name;

    public List<GameObject> Targets { get; set; } = new List<GameObject>();
    public string Name => _name;

    public void UseUIItem()
    {
        Execute(Entity.Null);
    }

    public void Execute(Entity entity)
    {
        foreach (var target in Targets)
        {
            if (_hpPerUse != 0)
            {
                var character = target.GetComponent<CharacterHealthConvertToEntity>();
                if (character == null) return;
                character.SetCharacterHealthEntity(_hpPerUse);
                Destroy(gameObject);
            }
        }
    }
}
