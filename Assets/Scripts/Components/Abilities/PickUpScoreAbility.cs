using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class PickUpScoreAbility : MonoBehaviour, IAbilityTarget, IConvertGameObjectToEntity
{
    public List<GameObject> Targets { get; set; } = new List<GameObject>();

    [SerializeField] private int _scoreForPickUp = 10;
    private EntityManager _dstManager;

    public void Execute(Entity entity)
    {
        foreach (var target in Targets)
        {
            var character = target.GetComponent<CharacterData>();
            if (character != null)
            {
                character.AddScore(_scoreForPickUp);
                _scoreForPickUp = 0;
                Destroy(gameObject);
                if (entity != Entity.Null) _dstManager.DestroyEntity(entity);
            }
        }
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        _dstManager = dstManager;
    }
}
