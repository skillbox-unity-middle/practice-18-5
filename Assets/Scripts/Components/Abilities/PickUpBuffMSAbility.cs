using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class PickUpBuffMSAbility : MonoBehaviour, IAbilityTarget, IConvertGameObjectToEntity
{
    [SerializeField] private float _buffMoveSpeedMultiplier = 2.0f;
    private EntityManager _dstManager;

    public List<GameObject> Targets { get; set; }

    public void Execute(Entity entity)
    {
        foreach (var target in Targets)
        {
            if (_buffMoveSpeedMultiplier != 0)
            {
                var character = target.GetComponent<CharacterConvertToEntity>();
                if (character == null) return;
                character.SpeedMultiplier = _buffMoveSpeedMultiplier;
                _buffMoveSpeedMultiplier = 0;
            }
            Destroy(gameObject);
            _dstManager.DestroyEntity(entity);
        }
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        _dstManager = dstManager;
    }
}
