﻿using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using static UnityEngine.EventSystems.EventTrigger;
using Unity.Transforms;
using Unity.Mathematics;

public class ApplyToWall : MonoBehaviour, IAbilityTarget, IConvertGameObjectToEntity
{
    public List<GameObject> Targets { get; set; }

    private EntityManager _dstManager;

    public void Execute(Entity entity)
    {
        bool isRotated = _dstManager.GetComponentData<RotatedData>(entity).isRotated;
        if (!isRotated)
        {
            foreach (var target in Targets)
            {
                if (target.tag.Equals("Wall"))
                {
                    bool hasBullet = _dstManager.HasComponent<BulletConvertToEntity>(entity);
                    if (hasBullet)
                    {
                        Rotation rotation = _dstManager.GetComponentData<Rotation>(entity);
                        Vector3 vector3FromRotation = new Vector3(rotation.Value.value.x, rotation.Value.value.y, rotation.Value.value.z);
                        Vector3 newVector3FromRotation =  Vector3.Reflect(vector3FromRotation, vector3FromRotation.normalized);
                        Quaternion newRotation = new Quaternion(newVector3FromRotation.x, newVector3FromRotation.y, newVector3FromRotation.z, rotation.Value.value.w);

                        _dstManager.SetComponentData(entity, new Rotation()
                        {
                            Value = newRotation
                        });

                        _dstManager.SetComponentData(entity, new RotatedData()
                        {
                            isRotated = true
                        });
                    }
                }
            }
        }
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        _dstManager = dstManager;
        dstManager.AddComponentObject(entity, this);
    }
}

