using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;

public class CharacterConvertToEntity : MonoBehaviour, IConvertGameObjectToEntity
{
    [SerializeField] private float2 _speed = new float2(0.1f, 0.1f);

    private Entity _entity;
    private EntityManager _dstManager;

    public float2 SpeedMultiplier { 
        set
        {
            _speed *= value;
            _dstManager.AddComponentData(_entity, new MoveData
            {
                speed = _speed
            });
        }
    }

    public MonoBehaviour dashAction;
    public MonoBehaviour shootAction;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponentData(entity, new InputData());
        dstManager.AddComponentData(entity, new ExistInputData());
        dstManager.AddComponentData(entity, new MoveData { 
            speed = _speed
        });
        if(dashAction != null && dashAction is IAbility)
        {
            dstManager.AddComponentData(entity, new DashData() { 
                isDashAvalaible = true
            });
        }
        if (shootAction != null && shootAction is IAbility)
        {
            dstManager.AddComponentData(entity, new ShootData());
        }
        _entity = entity;
        _dstManager = dstManager;
    }
}
