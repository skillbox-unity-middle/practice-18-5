using Unity.Entities;
using UnityEngine;
using Zenject;

public class CharacterHealthConvertToEntity : MonoBehaviour, IConvertGameObjectToEntity
{
    [SerializeField] private int _currentHealth = 0;
    [SerializeField] private int _maxHealth = 0;
    private EntityManager _dstManager;
    private Entity _entity;
    private ISettings _settings;
    private ViewModel _viewModel;

    [Inject]
    public void Init(ISettings settings)
    {
        _settings = settings;
    }

    private void Awake()
    {
        _viewModel = FindObjectOfType<ViewModel>();
        _currentHealth = _maxHealth;
        _settings.onLoadSettings += SetMaxHpFromSettings;
    }

    public void SetCharacterHealthEntity(int hp)
    {
        if (_currentHealth + hp >= _maxHealth) _currentHealth = _maxHealth;
        else _currentHealth += hp;
        SetEntityData();
    }

    public void SetMaxHp(int hp)
    {
        int newMaxHp = _maxHealth + hp;
        if (_currentHealth == _maxHealth) _currentHealth = newMaxHp;
        _maxHealth = newMaxHp;
        SetEntityData();
    }

    private void SetMaxHpFromSettings()
    {
        if (_currentHealth == _maxHealth) _currentHealth = _settings.HeroHealth;
        _maxHealth = _settings.HeroHealth;
        SetEntityData();
    }

    private void SetEntityData()
    {
        if (_viewModel != null)
        {
            _viewModel.Health = _currentHealth.ToString();
        }

        if (_dstManager.HasComponent<CharacterHealthData>(_entity))
        {
            _dstManager.SetComponentData(_entity, new CharacterHealthData
            {
                currentHealth = _currentHealth,
                maxHealth = _maxHealth
            });
        }
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponentData(entity, new CharacterHealthData
        {
            currentHealth = _currentHealth,
            maxHealth = _maxHealth
        });

        _entity = entity;
        _dstManager = dstManager;
    }
}
