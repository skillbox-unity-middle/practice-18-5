using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class AnimDataConvertToEntity : MonoBehaviour, IConvertGameObjectToEntity
{
    public string speedAnimHash;
    public string dashAnimHash;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        if (speedAnimHash != string.Empty && dashAnimHash != string.Empty)
        {
            dstManager.AddComponentData(entity, new AnimData());
        }

    }
}
