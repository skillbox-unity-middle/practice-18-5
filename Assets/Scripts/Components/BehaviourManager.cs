using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class BehaviourManager : MonoBehaviour, IConvertGameObjectToEntity
{
    [SerializeField] private List<MonoBehaviour> _behaviours;
    public List<IBehaviour> iBehaviours = new List<IBehaviour>();
    public IBehaviour currentBehaviour;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        foreach (var behaviour in _behaviours)
        {
            if (behaviour is IBehaviour iBehaviour)
            {
                iBehaviours.Add(iBehaviour);
            }
        }

        dstManager.AddComponentData(entity, new AIAgent());
    }

}
