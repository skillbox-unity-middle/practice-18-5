﻿public interface ICraftable
{
    string Name { get; }
}