﻿using System;

public interface ISettings
{
    public int HeroHealth { get; }
    public int RecoverHealth { get; }
    public event Action onLoadSettings; 
}

