﻿using Unity.Entities;

public interface IAbility
{
    public void Execute(Entity entity);
}

